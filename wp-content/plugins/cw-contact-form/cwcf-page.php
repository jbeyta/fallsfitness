<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class cw_contact_form_Admin {
	/**
 	 * Option key, and option page slug
 	 * @var string
 	 */
	private $key = 'cw_contact_form_options';
	/**
 	 * Options page metabox id
 	 * @var string
 	 */
	private $metabox_id = 'cw_contact_form_option_metabox';
	/**
	 * Options Page title
	 * @var string
	 */
	protected $title = '';
	/**
	 * Options Page hook
	 * @var string
	 */
	protected $options_page = '';
	/**
	 * Constructor
	 * @since 0.1.0
	 */
	public function __construct() {
		// Set our title
		$this->title = __( 'Contact Form', 'cw_contact_form' );
	}
	/**
	 * Initiate our hooks
	 * @since 0.1.0
	 */
	public function hooks() {
		add_action( 'admin_init', array( $this, 'init' ) );
		add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		add_action( 'cmb2_init', array( $this, 'add_options_page_metabox' ) );
	}
	/**
	 * Register our setting to WP
	 * @since  0.1.0
	 */
	public function init() {
		register_setting( $this->key, $this->key );
	}
	/**
	 * Add menu options page
	 * @since 0.1.0
	 */
	public function add_options_page() {
		$this->options_page = add_menu_page( $this->title, $this->title, 'update_core', $this->key, array( $this, 'admin_page_display' ), 'dashicons-editor-table' );
		// add_action( "admin_head-{$this->options_page}", array( $this, 'enqueue_js' ) );
	}
	/**
	 * Admin page markup. Mostly handled by CMB2
	 * @since  0.1.0
	 */
	public function admin_page_display() {
		?>
		<div class="wrap cmb2-options-page <?php echo $this->key; ?>">
			<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
			<p>Select the desired fields fill in the captcha info below. Create a page and use the shortcode (below) where you want the form to show up. NOTE: If you wish to use another form plugin (i.e. Gravity forms, Contact Form 7, etc.), deselect and delete everything on this page to avoid conflicts and confusion.</p>
			<hr>
			<h3>Shortcode:</h3>
			<p>[contact-form]</p>

			<h4 class="sc-options-title">Click for Shortcode Options</h4>
			<p class="sc-options">
				You can put field parameters in the shortcode to override options on this page.<br>
				<br>
				<b>Form Title:</b>
				<br>[contact-form title="Example Title"]
				<br>
				<br>
				<b>Show Field Types:</b>
				<br>[contact-form name="true" email="true" phone="true" address="true" message="true"]
				<br>
				<br>
				<b>Hide Field Types:</b>
				<br>[contact-form name="false" email="false" phone="false" address="false" message="false"]
				<br>
				<br>
				<b>Placeholders:</b>
				<br>[contact-form use_placeholders="true"] or [contact-form use_placeholders="false"]
				<br>
				<br>
				<b>Custom Field:</b>
				<br>[contact-form custom="Example Text Input"]
				<br>
				<br>
				<b>Recipient:</b>
				<br>[contact-form recipient="example@email.com"]
			</p>
			<hr>
			<?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
		</div>
		<script>
			jQuery('.sc-options-title').click(function(){
				console.log('click');
				jQuery('.sc-options').slideToggle();
			});
		</script>
		<?php
	}
	/**
	 * Add the options metabox to the array of metaboxes
	 * @since  0.1.0
	 */
	function add_options_page_metabox() {
		$prefix = '_ccfo_';

		$cmb = new_cmb2_box( array(
			'id'	  => $this->metabox_id,
			'hookup'  => false,
			'show_on' => array(
				// These are important, don't remove
				'key'   => 'options-page',
				'value' => array( $this->key, )
			),
		) );

		// Set our CMB2 fields
		$cmb->add_field( array(
			'name' => 'Form Title',
			'id' => $prefix.'form_title',
			'type' => 'text'
		) );

		$cmb->add_field( array(
			'name' => 'Select Fields to display',
			'desc' => 'If no fields are selected, form will not be shown.',
			'id' => $prefix.'fields',
			'type' => 'multicheck',
			'options' => array(
				'name' => 'Name',
				'email' => 'Email',
				'phone' => 'Phone',
				'address' => 'Address',
				'message' => 'Message'
			)
		) );

		$cmb->add_field( array(
			'name' => '&nbsp;',
			'desc' => 'Use placeholders instead of labels?',
			'id' => $prefix.'placeholders',
			'type' => 'checkbox'
		) );

		$cmb->add_field( array(
			'name' => 'Custom Field Label',
			'desc' => 'If you wanted to add one custom field, that would be alright. Just put in a name here and it will show up in for form.
						<br>The custom field will not be required and will be a text input.
						<br>NOTE: Custom Field Label must include at least one letter or number',
			'id' => $prefix.'custom_field',
			'type' => 'text'
		) );

		$cmb->add_field( array(
			'name' => 'From Email Address',
			'desc' => 'NOTE: If this is blank, Submit button will be disabled and forms will not be able to send.',
			'id' => $prefix.'from_email',
			'type' => 'text_email'
		) );

		$cmb->add_field( array(
			'name' => 'Recipient Email Address',
			'desc' => 'NOTE: If this is blank, Submit button will be disabled and forms will not be able to send.',
			'id' => $prefix.'send_to_email',
			'type' => 'text_email'
		) );

		$cmb->add_field( array(
			'name' => 'Email Subject',
			'desc' => 'NOTE: If this is blank a default subject will be used. URL of website and sender\'s name will be included in custom subject.',
			'id' => $prefix.'subject',
			'type' => 'text'
		) );

		$cmb->add_field( array(
			'name' => 'Confirmation Message',
			'desc' => 'NOTE: If left empty, a default message will be used: Your message has been sent. We will be back in touch with you shortly.',
			'id' => $prefix.'conf',
			'type' => 'textarea'
		) );

		$cmb->add_field( array(
			'name' => 'Google reCAPTCHA Site Key',
			'id' => $prefix.'recap_site_key',
			'type' => 'text'
		) );
		$cmb->add_field( array(
			'name' => 'Google reCAPTCHA Secret key',
			'desc' => 'NOTE: If this is empty, form will not be shown.<br>Obtain reCAPTCHA keys here <a href="https://www.google.com/recaptcha/">https://www.google.com/recaptcha/</a>',
			'id' => $prefix.'recap_secret_key',
			'type' => 'text'
		) );

		$cmb->add_field( array(
			'name' => '&nbsp;',
			'desc' => 'Hide reCAPTCHA until user starts using the form',
			'id' => $prefix.'hide_captcha',
			'type' => 'checkbox'
		) );
	}
	/**
	 * Public getter method for retrieving protected/private variables
	 * @since  0.1.0
	 * @param  string  $field Field to retrieve
	 * @return mixed Field value or exception is thrown
	 */
	public function __get( $field ) {
		// Allowed fields to retrieve
		if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
			return $this->{$field};
		}
		throw new Exception( 'Invalid property: ' . $field );
	}
}

/**
 * Helper function to get/return the cw_contact_form_Admin object
 * @since  0.1.0
 * @return cw_contact_form_Admin object
 */
function cw_contact_form_admin() {
	static $object = null;
	if ( is_null( $object ) ) {
		$object = new cw_contact_form_Admin();
		$object->hooks();
	}
	return $object;
}
/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed		Option value
 */
function cw_contact_form_get_option( $key = '' ) {
	// global $cw_contact_form_Admin;
	// return cmb2_get_option( myprefix_admin()->key, $key );
	// $cw_contact_form = get_option('cw_contact_form_options');
	$cw_contact_form = get_option(cw_contact_form_admin()->key);
	return $cw_contact_form[$key];

}
// Get it started
cw_contact_form_admin();