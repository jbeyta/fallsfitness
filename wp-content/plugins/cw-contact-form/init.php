<?php
/*
// Plugin Name: CW Contact Form
// Plugin URI: http://crane-west.com
// Description: Dirt simple, light-weight contact form plugin. NOTE: Requires CMB2 plugin & jquery. Falls back to included cmb2 files, but may need update if plugin is not installed.
// Version: 1.0
// Author: Joel Abeyta, Crane | West
// Author URI: http://crane-west.com
// License: GPLv2 or later
 */
///////////////////////////////////////////////// Contact Form /////////////////////////////////////////////////

// fall back if cmb2 plugin is not installed: may need an update from here -> https://github.com/WebDevStudios/CMB2
if ( file_exists( plugin_dir_path( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once plugin_dir_path( __FILE__ ) . '/cmb2/init.php';
}

include( plugin_dir_path( __FILE__ ) . 'cwcf-page.php');
include( plugin_dir_path( __FILE__ ) . 'cwcf-shortcode.php');

function process_form($fields, $post_values, $to, $custom_title = '', $custom = '', $custom_subject = '') {
	$empty_inputs = array();
	$error_inputs = array();
	// echo_pre($post_values);
	// checking if the fields are empty
	foreach($fields as $field) {
		if($field == 'address') {
			if(empty($post_values['cf_address_1'])) {
				array_push($empty_inputs, 'cf_address_1');
			}

			if(empty($post_values['cf_city'])) {
				array_push($empty_inputs, 'cf_city');
			}

			if(empty($post_values['cf_state'])) {
				array_push($empty_inputs, 'cf_state');
			}

			if(empty($post_values['cf_zip'])) {
				array_push($empty_inputs, 'cf_zip');
			}
		} else {
			$name = 'cf_'.$field;

			if(empty($post_values[$name])) {
				array_push($empty_inputs, $name);
			}
		}
	}

	// make sure we get a valid email address
	if (!filter_var($post_values['cf_email'], FILTER_VALIDATE_EMAIL)) {
		array_push($error_inputs, 'cf_email');
	}

	// recaptcha stuff
	require_once "recaptchalib.php";
	$recap_secret_key = cw_contact_form_get_option('_ccfo_recap_secret_key');
	$response = NULL;

	// check secret key
	$reCaptcha = new ReCaptcha($recap_secret_key);

	// variable for form field error messages
	$recap_success = '';

	// check response
	if($post_values["g-recaptcha-response"]) {
		$response = $reCaptcha->verifyResponse(
			$_SERVER["REMOTE_ADDR"],
			$post_values["g-recaptcha-response"]
		);
	} else {
		$recap_success = 'empty';
	}

	if($response != null && $response->success) {
		$recap_success = 'success';
	} elseif($response == null) {
		$recap_success = 'recap_empty';
	} elseif(!$response->success) {
		$recap_success = 'recap_fail';
	}

	// if nothing is empty and recaptcha is success, send that email
	if($recap_success == 'success') {
		$send_email = cw_contact_form_send($post_values, $to, $from, $custom_title, $custom, $custom_subject);
		if(empty($empty_inputs) && empty($empty_inputs)) {
			if($send_email == 'success') {
				return 'success';
			} else {
				return 'wp_mail_error';
			}
		} else {
			return array(
				'empty_inputs' => $empty_inputs,
				'error_inputs' => $error_inputs,
				'recap_success' => $recap_success,
			);
		}
	} else {
		return $recap_success;
	}
}

function cw_contact_form_send($fields, $to, $from, $custom_title = '', $custom = '', $custom_subject = '') {
	$name = $fields['cf_name'];
	$email = $fields['cf_email'];
	$phone = $fields['cf_phone'];
	$address_1 = $fields['cf_address_1'];
	$address_2 = $fields['cf_address_2'];
	$city = $fields['cf_city'];
	$state = $fields['cf_state'];
	$zip = $fields['cf_zip'];
	$comment = $fields['cf_message'];

	$site_name = get_bloginfo('wpurl');

	if(!empty($custom_subject)) {
		$subject = $custom_subject.' - from '.$name.', '.$site_name;
	} else {
		$subject = 'New message from '.$name.', '.$site_name;
	}

	$headers = "From: ".$from." <".$from.">\r\n";
	$headers .= "Reply-To: ".$name." <".$email.">\r\n";
	$headers .= "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-Type: text/plain; charset=utf-8\r\n"; 
	$headers .= "X-Priority: 1\r\n";

	$message = '';

	if(!empty($name)) {
		$message .= $name."\r\n";
	}

	if(!empty($email)) {
		$message .= $email."\r\n";
	}

	if(!empty($phone)) {
		$message .= $phone."\r\n";
	}

	if(!empty($address_1) || !empty($address_2)) {
		$message .= $address_1." ".$address_2."\r\n";
	}

	if(!empty($city) || !empty($state) || !empty($zip)) {
		$message .= $city.", ".$state." ".$zip."\r\n\r\n";
	}

	if(!empty($custom)) {
		$message .= $custom_title.': '.$custom."\r\n";
	}

	if(!empty($comment)) {
		$message .= $comment."\r\n";
	}

	$sent = wp_mail($to, $subject, $message, $headers);

	if($sent) {
		return 'success';
	} else {
		return 'error';
	}
}

add_action("wp_enqueue_scripts", "recaptcha_js", 11);
function recaptcha_js() {
	wp_enqueue_script('recaptcha', 'https://www.google.com/recaptcha/api.js', array(), '1', true);
}

add_action("wp_enqueue_scripts", "cwcf_enqueue_frontend", 11);
function cwcf_enqueue_frontend() {
	$mtime = filemtime(plugin_dir_path(__FILE__) . '/cwcf-style.css');
	wp_register_style( 'cwcf-stylesheet', plugins_url('cwcf-style.css', __FILE__) );
	wp_enqueue_style( 'cwcf-stylesheet' );
}

function cwcf_enqueue_admin() {
	if($_GET['page'] != 'cw_contact_form_options') {
		return;
	}
	$mtime = filemtime(plugin_dir_path(__FILE__) . '/cwcf-admin-style.css');
	wp_register_style( 'cwcf-admin-stylesheet', plugins_url('cwcf-admin-style.css', __FILE__) );
	wp_enqueue_style( 'cwcf-admin-stylesheet' );
}
add_action( 'admin_enqueue_scripts', 'cwcf_enqueue_admin' );