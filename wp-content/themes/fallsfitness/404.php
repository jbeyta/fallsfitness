<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="m8">
			<h2 class="entry-title">Not Found: 404</h2>
			<p>Content not found. Please make sure the url is correct.</p>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>