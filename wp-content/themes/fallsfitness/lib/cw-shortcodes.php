<?php
function cw_column( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'size' => '6',
		'screen' => 'm',
		'divider' => ''
	), $atts));

	$class = '';
	if(!empty($divider)) {
		$class = 'divider-'.$divider;
	}

	// clean up the content to remove extra <br> and <p></p> tags
	$content = preg_replace( '#^<\/p>|^<br \/>|<p>$#', '', $content );
	
	return '<div class="'.$screen.''.$size.' '.$class.'">'.$content.'</div>';
}
add_shortcode( 'cw_column', 'cw_column' );

function cw_columns( $atts, $content = null ) {
	extract(shortcode_atts(array(
		'number' => '2'
	), $atts));

	return '<div class="cw-columns-'.$number.'">'.$content.'</div>';
}
add_shortcode( 'cw_columns', 'cw_columns' );

// phone number short code
function cw_phone_umber( $atts, $content = null ) {
	global $post;

	ob_start();

		echo cw_contact_get_option('_cwc_phone');

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'phone_number', 'cw_phone_umber' );

function cw_posts_list( $atts, $content = null ) {
	global $post;

	ob_start();

		echo '<div class="article-list">';
			$posts = get_posts();

			foreach($posts as $post) {
				echo '<article class="news-article">';
					echo '<span class="close">X</span>';
					
					echo '<h4 class="article-title">';
						echo get_the_title($post->ID);
					echo '</h4>';

					$date = get_the_date('F j, Y');

					echo '<p class="article-date"><b>Posted '.$date.'</b></p>';

					$post_content = $post->post_content;
					$post_excerpt = strip_tags(substr($post_content, 0, 100));

					echo '<p class="article-excerpt">';
						echo $post_excerpt.'...<br><span class="readmore">Read More</span>';
					echo '</p>';

					echo '<div class="article-content">';
						echo get_the_post_thumbnail($post->ID);
						echo '<p>'.$post_content.'</p>';
					echo '</div>';

				echo '</article>';
			}
			wp_reset_query();

		echo '</div>';

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'get_posts', 'cw_posts_list' );

function cw_get_promos( $atts, $content = null ) {
	global $post;

	extract(shortcode_atts(array(
		'position' => '',
		'orderby' => '',
		'order' => ''
	), $atts));

	ob_start();

		cw_get_promo($position);

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'get_promo', 'cw_get_promos' );

function cw_get_address( $atts, $content = null ) {
	global $post;

	extract(shortcode_atts(array(
		'title' => '',
		'address' => 'show',
		'style' => '',
		'phone' => '',
		'phone2' => '',
		'fax' => '',
		'email' => '',
		'hours' => '',
		'social' => '',
		'map' => '',
		'map_width' => '100%',
		'map_height' => '300px',
		'icon' => false,
		'show_all' => false
	), $atts));

	ob_start();

		$cwc_address1 = cw_contact_get_option( '_cwc_address1' );
		$cwc_address2 = cw_contact_get_option( '_cwc_address2' );
		$cwc_city = cw_contact_get_option( '_cwc_city' );
		$cwc_state = cw_contact_get_option( '_cwc_state' );
		$cwc_zip = cw_contact_get_option( '_cwc_zip' );

		if($map == 'show' || $show_all == true) {
			$hide_controls = cw_contact_get_option( '_cwc_hide_controls' );
			$allow_scroll_zoom = cw_contact_get_option( '_cwc_allow_scroll_zoom' );
			$zoom_level = cw_contact_get_option( '_cwc_zoom_level' );
			$saturation = intval( cw_contact_get_option( '_cwc_saturation' ) );
			$simplified = cw_contact_get_option( '_cwc_simplified' );
			$hue_hex = cw_contact_get_option( '_cwc_hue_hex' );

			$controls = 'false';
			if(!empty($hide_controls) && $hide_controls == 'on') {
				$controls = 'true';
			} else {
				$controls = 'false';
			}

			$scroll_zoom = 'false';
			if(!empty($allow_scroll_zoom) && $allow_scroll_zoom == 'on') {
				$scroll_zoom = 'true';
			} else {
				$scroll_zoom = 'false';
			}

			$zoom = 14;
			if(!empty($zoom_level)) {
				$zoom = $zoom_level;
			}

			$satch = 0;
			if($saturation === 0) {
				$satch = -100;
			}
			if(!empty($saturation)) {
				$amount = $saturation - 100;
				$satch = $amount;
			}

			$visibility = 'on';
			if(!empty($simplified) && $simplified == 'on') {
				$visibility = 'simplified';
			}

			$hue = '';
			if(!empty($hue_hex)) {
				$hue = $hue_hex;
			}

			$map_id = (string) $post_id;
			if(empty($map_id)) {
				$map_id = '1';
			}

			$address_string = $cwc_address1.' '.$cwc_address2.' '.$cwc_city.', '.$cwc_state.' '.$cwc_zip;
			$address_url = urlencode($address_string);
			echo '<input id="address_'.$map_id.'" type="hidden" value="'.$address_string.'">';

			$details_url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$address_url."&sensor=false";
			 
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $details_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = json_decode(curl_exec($ch), true);
			 
			// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST
			if ($response['status'] != 'OK') {
				return null;
			}
			 
			$geometry = $response['results'][0]['geometry'];
			$latitude = $geometry['location']['lat'];
			$longitude = $geometry['location']['lng']; ?>

			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVM4QradvqQ_Ev__GTL4_2DJ6Lc2W9__I&v=3.exp&sensor=false"></script>
				<script type="text/javascript">
					var geocoder_<?php echo $map_id; ?>;
					var map_<?php echo $map_id; ?>;

					function initialize_<?php echo $map_id; ?>() {
						var customMapType_<?php echo $map_id; ?> = new google.maps.StyledMapType([
							{
								stylers: [
								{hue: '<?php echo $hue; ?>'},
								{visibility: '<?php echo $visibility; ?>'},
								{saturation: <?php echo $satch; ?>},
								]
							}
							], {
							name: 'Custom Style'
						});
						var customMapTypeId_<?php echo $map_id; ?> = 'custom_style';

						geocoder_<?php echo $map_id; ?> = new google.maps.Geocoder();
						var latlng_<?php echo $map_id; ?> = new google.maps.LatLng(<?php echo $latitude.', '.$longitude; ?>);
						var mapOptions_<?php echo $map_id; ?> = {
							zoom: <?php echo $zoom; ?>,
							center: latlng_<?php echo $map_id; ?>,
							scrollwheel: <?php echo $scroll_zoom; ?>,
							disableDefaultUI: <?php echo $controls; ?>
						}

						map_<?php echo $map_id; ?> = new google.maps.Map(document.getElementById('map-canvas-'+<?php echo $map_id; ?>), mapOptions_<?php echo $map_id; ?>);

						map_<?php echo $map_id; ?>.mapTypes.set(customMapTypeId_<?php echo $map_id; ?>, customMapType_<?php echo $map_id; ?>);
						map_<?php echo $map_id; ?>.setMapTypeId(customMapTypeId_<?php echo $map_id; ?>);
					}

					function codeAddress_<?php echo $map_id; ?>() {
						var address_<?php echo $map_id; ?> = document.getElementById('address_'+<?php echo $map_id; ?>).value;
						geocoder_<?php echo $map_id; ?>.geocode( { 'address': address_<?php echo $map_id; ?>}, function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								map_<?php echo $map_id; ?>.setCenter(results[0].geometry.location);
								var marker_<?php echo $map_id; ?> = new google.maps.Marker({
									map: map_<?php echo $map_id; ?>,
									position: results[0].geometry.location
								});
							} else {
								alert('Geocode was not successful for the following reason: ' + status);
							}
						});
					}

					google.maps.event.addDomListener(window, 'load', initialize_<?php echo $map_id; ?>);
					google.maps.event.addDomListener(window, 'load', codeAddress_<?php echo $map_id; ?>);
				</script>
			<?php

			echo '<div id="map-canvas-'.$map_id.'" style="height: '.$map_height.'; width: '.$map_width.';"></div>';
			// echo '<a class="map" href="http://maps.google.com/maps?saddr=&daddr='.$address_url.'" title="Get Directions" target="_blank"><small>Click for Directions</small></a>';
		}

		if($address == 'show' || $show_all == true) {
			if($style == 'inline') {
				echo '<p>';
					if(!empty($title)) { echo $title.'&nbsp;'; }
					if(!empty($cwc_address1)) { echo $cwc_address1; }
					if(!empty($cwc_address2)) { echo '&nbsp;&bull;&nbsp;'.$cwc_address2; }
					if(!empty($cwc_city)) { echo '&nbsp;&bull;&nbsp;'.$cwc_city; }
					if(!empty($cwc_state) && !empty($cwc_city)) { echo ',';}
					if(!empty($cwc_state)) { echo ' '.$cwc_state; }
					if(!empty($cwc_zip)) { echo ' '.$cwc_zip; }
				echo '</p>';
			} else {
				echo '<p>';
					if(!empty($title)) { echo $title.'<br>'; }
					if(!empty($cwc_address1)) { echo $cwc_address1; }
					if(!empty($cwc_address2)) { echo '<br>'.$cwc_address2; }
					if(!empty($cwc_city)) { echo '<br>'.$cwc_city; }
					if(!empty($cwc_state) && !empty($cwc_city)) { echo ',';}
					if(!empty($cwc_state)) { echo ' '.$cwc_state; }
					if(!empty($cwc_zip)) { echo ' '.$cwc_zip; }
				echo '</p>';
			}
		}

		// phone
		if($phone == 'show' || $show_all == true) {
			$cwc_phone = cw_contact_get_option( '_cwc_phone' );

			if(!empty($cwc_phone)) {
				if($icon == true) {
					echo '<p class="phone"><i class="fa fa-phone" aria-hidden="true"></i>  <a href="tel:'.$cwc_phone.'">'.$cwc_phone.'</a></p>';
				} else {
					echo '<p class="phone"><a href="tel:'.$cwc_phone.'">'.$cwc_phone.'</a></p>';
				}
			}
		}

		if($phone2 == 'show' || $show_all == true) {
			$cwc_phone2 = cw_contact_get_option( '_cwc_phone2' );

			if(!empty($cwc_phone2)) {
				if($icon == true) {
					echo '<p><i class="fa fa-phone" aria-hidden="true"></i>  <a href="tel:'.$cwc_phone2.'">'.$cwc_phone2.'</a></p>';
				} else {
					echo '<p><a href="tel:'.$cwc_phone2.'">'.$cwc_phone2.'</a></p>';
				}
			}
		}

		// fax
		if($fax == 'show' || $show_all == true) {
			$cwc_fax = cw_contact_get_option( '_cwc_fax' );

			if(!empty($cwc_fax)) {
				if($icon == true) {
					echo '<p><i class="fa fa-fax" aria-hidden="true"></i>  <span>'.$cwc_fax.'</span></p>';
				} else {
					echo '<p><span>'.$cwc_fax.'</span></p>';
				}
			}
		}

		// email
		if($email == 'show' || $show_all == true) {
			$cwc_email = cw_contact_get_option( '_cwc_email' );

			if(!empty($cwc_email)) {
				if($icon == true) {
					echo '<p><i class="fa fa-envelope-o" aria-hidden="true"></i>  <a href="mailto:'.$cwc_email.'">'.$cwc_email.'</a></p>';
				} else {
					echo '<p><a href="mailto:'.$cwc_email.'">'.$cwc_email.'</a></p>';
				}
			}
		}

		// hours
		if($hours == 'show' || $show_all == true) {
			$cwc_hours = cw_contact_get_option('_cwc_hours');

			if(!empty($cwc_hours)) {
				if($icon == true) {
					echo '<p class="sc-hours"><i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>'.nl2br($cwc_hours).'</p>';
				} else {
					echo '<p class="hours">'.nl2br($cwc_hours).'</p>';
				}
			}
		}

		// social
		if($social == 'facebook' || $show_all == true){
			$cwc_facebook = cw_contact_get_option( '_cwc_facebook' );

			if(!empty($cwc_facebook)) {
				echo '<a href="'.$cwc_facebook.'"><i class="fa fa-facebook-official fa-2x"></i></a>';
			}
		}

		if($social == 'twitter' || $show_all == true){
			$cwc_twitter = cw_contact_get_option( '_cwc_twitter' );

			if(!empty($cwc_twitter)) {
				echo '<a href="'.$cwc_twitter.'"><i class="fa fa-twitter fa-2x"></i></a>';
			}
		}

		if($social == 'googleplus' || $show_all == true){
			$cwc_gplus = cw_contact_get_option( '_cwc_gplus' );

			if(!empty($cwc_gplus)) {
				echo '<a href="'.$cwc_gplus.'"><i class="fa fa-google-plus fa-2x"></i></a>';
			}
		}

		if($social == 'youtube' || $show_all == true){
			$cwc_youtube = cw_contact_get_option( '_cwc_youtube' );

			if(!empty($cwc_youtube)) {
				echo '<a href="'.$cwc_youtube.'"><i class="fa fa-youtube fa-2x"></i></a>';
			}
		}

		if($social == 'linkedin' || $show_all == true){
			$cwc_linked = cw_contact_get_option( '_cwc_linked' );

			if(!empty($cwc_linked)) {
				echo '<a href="'.$cwc_linked.'"><i class="fa fa-linkedin fa-2x"></i></a>';
			}
		}

		if($social == 'pinterest' || $show_all == true){
			$cwc_pinterest = cw_contact_get_option( '_cwc_pinterest' );

			if(!empty($cwc_pinterest)) {
				echo '<a href="'.$cwc_pinterest.'"><i class="fa fa-pinterest fa-2x" aria-hidden="true"></i></a>';
			}
		}

		if($social == 'instagram' || $show_all == true){
			$cwc_instagram = cw_contact_get_option( '_cwc_instagram' );

			if(!empty($cwc_instagram)) {
				echo '<a href="'.$cwc_instagram.'"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>';
			}
		}

	$temp = ob_get_contents();
	ob_end_clean();

	return $temp;
}
add_shortcode( 'contact_info', 'cw_get_address' );