<?php
	$qlinks = cw_quicklinks_get_option( 'cwo_quicklinks' );

	if(!empty($qlinks)) {
		// echo_pre($qlinks);
		// $count = count($qlinks);
		echo '<ul class="quicklinks styleless lload lchildren">';
		foreach ($qlinks as $qlink) {
			if(!empty($qlink['ql_page']) || !empty($qlink['ql_url']) || !empty($qlink['ql_file'])) {
				$qlbg = '';

				if(!empty($qlink['ql_page'])) {
					$img_url = wp_get_attachment_url( get_post_thumbnail_id($qlink['ql_page']) );
					$cropped = aq_resize($img_url, 640, 640, true, true, true);
					if(empty($cropped)) {
						$cropped = $img_url;
					}
					$qlbg = '<img src="'.$cropped.'" alt="" />';
				}

				echo '<li class="quicklink lchild lzoomin">';
				// echo_pre($qlink);
				if(!empty($qlink['ql_login'])) {
					echo '<a class="inner" href="'.wp_login_url().'">';
				} elseif(!empty($qlink['ql_url'])) {
					echo '<a class="inner" href="'.$qlink['ql_url'].'">';
				} elseif(!empty($qlink['ql_page'])) {
					echo '<a class="inner" href="'.get_the_permalink($qlink['ql_page']).'">';
				} elseif(!empty($qlink['ql_file'])) {
					$file = get_post($qlink['ql_file']);
					echo '<a class="inner" href="'.$file->guid.'">';
				}
			}

			$title = '';

			if(!empty($qlink['ql_title'])) {
				$title = $qlink['ql_title'];
			} elseif(!empty($qlink['ql_page'])) {
				$title = get_the_title($qlink['ql_page']);
			} elseif(!empty($qlink['ql_file'])) {
				$title = get_the_title($qlink['ql_file']);
			}

			echo '<h5 class="button-title">'.$title.'</h5>';
			echo $qlbg;
			
			if(!empty($qlink['ql_page']) || !empty($qlink['ql_url']) || !empty($qlink['ql_file'])) {
				echo '</a></li>'; // end inner
			}
		}
		echo '</ul>';
	}
?>