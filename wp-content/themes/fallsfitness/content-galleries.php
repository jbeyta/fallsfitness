<?php
	$images = get_post_meta($post->ID, '_cwmb_gallery_images', true);
	$desc = get_post_meta($post->ID, '_cwmb_gallery_desc', true);

	$images = array_values($images);

	$contents = '';

	$contents .= '<h4 class="gallery-title">'.get_the_title().'</h4>';

	if(!empty($desc)) {
		$excerpt = cw_excerpt($desc, 25);
		$contents .= '<p class="gallery-desc">'.nl2br($excerpt).'&hellip;</p>';
	}

	if(!empty($images)) {
		echo '<div class="gallery-listing m4" style="background-image: url('.$images[0].');"><a href="'.get_the_permalink().'">';
			echo $contents;
		echo '</a></div>';
	}