<?php
	$images = get_post_meta($post->ID, '_cwmb_gallery_images', true);
	$desc = get_post_meta($post->ID, '_cwmb_gallery_desc', true);

	if(!empty($desc)) {
		echo '<p>'.nl2br($desc).'</p>';
	}

	if(!empty($images)) {
		echo '<div class="row">';
		foreach ($images as $img) {
			$cropped = aq_resize($img, 300, 300, true, true, true);

			echo '<div class="s6 m4 l3 end">';
				echo '<a href="'.$img.'" data-lightbox="gallery-'.$post->ID.'">';
					echo '<img class="gallery-image" src="'.$cropped.'" alt="" />';
				echo '</a>';
			echo '</div>';
		}
		echo '</div>';
	}