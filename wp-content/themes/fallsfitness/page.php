<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>

	<div class="main row" role="main">
		<div class="m8">
			<?php
				if(have_posts()) {
					while(have_posts()) {
						the_post();
						echo '<h2 class="page-title">'.get_the_title().'</h2>';
						the_content();
						// comments_template( '', true );
					}
				}
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>