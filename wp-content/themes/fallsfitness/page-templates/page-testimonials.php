<?php
/**
 * Template Name: Testimonials Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="testimonials row" role="main">
		<div class="m8">
			<?php
				echo '<h2 class="page-title">'.get_the_title().'</h2>';

				if (have_posts()) : while (have_posts()) : the_post();
					the_content();
				endwhile; endif;
			?>
		
			<?php 
				$post_type = 'testimonials';
				$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$post_args = array(
					'post_type' => $post_type,
					'posts_per_page' => 10,
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'paged' => $paged
				);

				$posts = new WP_Query($post_args);
				if($posts->have_posts()){
					while($posts->have_posts()){
						$posts->the_post();

						get_template_part('content', $post_type);
					}

					if (function_exists('pagination')) {
						pagination($posts->max_num_pages);
					}
				} else {
					echo '<p>No '.$post_type.' yet. Check back soon</p>';
				}

				wp_reset_query();
			?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>