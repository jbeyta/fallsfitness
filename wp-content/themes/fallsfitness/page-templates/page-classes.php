<?php
/**
 * Template Name: Classes Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header();
$this_page = get_permalink($post->ID);

?>
	<div class="schedule-container row" role="main">
		<div class="m8">
			<h2 class="page-title"><?php the_title();?></h2>
			
			<?php if (have_posts()) : while (have_posts()) : the_post();
				the_content();
			endwhile; endif; ?>

			<?php 
				$taxonomy = 'classes_categories';

				$post_type = 'classes';
				$post_args = array(
					'post_type' => $post_type,
					'posts_per_page' => -1,
					'orderby' => 'date',
					'order' => 'DESC'
				);

				if(isset($_GET['category']) && !empty($_GET['category'])) {
					$post_args['tax_query'] = array(
						array(
							'taxonomy' => $taxonomy,
							'field' => 'slug',
							'terms' => $_GET['category']
						)
					);
				}

				$posts = new WP_Query($post_args);

				// search params
				$rooms = array();
				global $cw_locs_list;
				$terms = get_terms($taxonomy);

				$content_blocks = '';

				// output
				// this is all based on day of the week, so we'll go ahead and make keys for each day to match the meta values from the posts
				$output = array(
					'monday' => array(),
					'tuesday' => array(),
					'wednesday' => array(),
					'thursday' => array(),
					'friday' => array(),
					'saturday' => array(),
					'sunday' => array()
				);

				if($posts->have_posts()){
					while($posts->have_posts()){
						$posts->the_post();

						$days = get_post_meta($post->ID, '_cwmb_schedule_infos', true);
						$room_raw = get_post_meta($post->ID, '_cwmb_room', true);

						$video = esc_url( get_post_meta($post->ID, '_cwmb_video', true) );

						// clean up the room names so we can used them for a select and query vars... this is just in case someone gets smart and tries to put some html or get stupid and puts weird characters in there.  I think cmb2 is already doing some strip_tags or something but, can't hurt to double wrap that shit
						$room = ucfirst( strip_tags( $room_raw ) );
						$room = preg_replace("/[^A-Za-z0-9 ]/", '', $room);
						$room_lower = strtolower( str_replace(' ', '-', $room) );

						if(!in_array($room, $rooms)) {
							$rooms[$room_lower] = $room_raw;
						}

						// content blocks to be hidden and then called up when user clicks on title
						$content_blocks .= '<div id="'.$post->ID.'" class="sched-content-cover">';
							$content_blocks .= '<div class="sched-content">';
								$content_blocks .= '<span class="close-gal" data-target="'.$post->ID.'"><i class="fa fa-times" aria-hidden="true"></i></span>';
								$content_blocks .= '<h4>'.get_the_title().'</h4><hr>';
								$content_blocks .= '<p class="teacher liteBlue-text"></p>';
								$content_blocks .= get_the_content();

								if(!empty($video)) {
									$content_blocks .= '<div>';
									$content_blocks .= wp_oembed_get( $video );
									$content_blocks .= '</div>';
								}

								$content_blocks .= '<p><b>Room: </b>'.$room.'</p>';
							$content_blocks .= '</div>';
						$content_blocks .= '</div>';

						// filtering for rooms and locations which are meta.  Don't have to do it this way, but this works. Might change it later.
						$add_to_list = true;

						if(!empty($_GET['room']) && $_GET['room'] != $room_lower) {
							$add_to_list = false;
						}

						// if we're good to go and have some entries for days, build that shit
						if(!empty($days) && $add_to_list) {
							foreach($days as $day) {

								// under each day, the classes will be sorted by times with are entered in the admin in a text area, one time per line. Explode to make array
								$times = explode("\n", $day['times']);

								// loop through the times and make an array for each one with all relevant info
								if(!empty($times)) {
									$i=0;
									foreach($times as $time) {
										$output[$day['day']][$i.$post->ID] = array(
											'time' => trim($time),
											'title' => get_the_title(),
											'content' => $post->ID,
											'room' => $room_raw,
											'teacher' => $day['teacher']
										);

										$i++;
									}
								}

							} // foreach days
						} // if not empty days
					} // while have_posts

					// this is the filterer. runs on javascript below
					echo '<div class="row schedule-picker">';
						// build the selects from the stuff we made earlier
						// if(!empty($terms)) {
						// 	echo '<div class="m4">';
						// 		echo '<select class="sched" name="category">';
						// 			echo '<option value="">All Categories</option>';
									
						// 			foreach($terms as $term) {

						// 				$selected = '';
						// 				if(isset($_GET['category']) && $_GET['category'] == $term->slug) {
						// 					$selected = 'selected';
						// 				}

						// 				echo '<option value="'.$term->slug.'" '.$selected.'>'.$term->name.'</option>';
						// 			}
						// 		echo '</select>';
						// 	echo '</div>';
						// }

						// if(!empty($rooms)) {
						// 	echo '<div class="m4">';
						// 		echo '<select class="sched" name="room">';
						// 			echo '<option value="">All Rooms</option>';

						// 			foreach($rooms as $slug => $room) {

						// 				$selected = '';
						// 				if(isset($_GET['room']) && $_GET['room'] == $slug) {
						// 					$selected = 'selected';
						// 				}

						// 				echo '<option value="'.$slug.'" '.$selected.'>'.$room.'</option>';
						// 			}
						// 		echo '</select>';
						// 	echo '</div>';
						// }
						?>

						<script type="text/javascript">
							jQuery(document).ready(function($){
								// this is from the internet somewhere, parses the url for query strings
								function getParameterByName(name, url) {
									if (!url) {
										url = window.location.href;
									}
									name = name.replace(/[\[\]]/g, "\\$&");
									var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
										results = regex.exec(url);
									if (!results) return '';
									if (!results[2]) return '';
									return decodeURIComponent(results[2].replace(/\+/g, " "));
								}

								$('.sched').on('change', function(){
									var value = $(this).val();
									var name = $(this).attr('name');

									var room_value = '';
									var location_value = '';
									var category_value = '';

									room_value = getParameterByName('room');
									location_value = getParameterByName('location');
									category_value = getParameterByName('category');

									if(name == 'room') {
										room_value = value;
									}

									if(name == 'location') {
										location_value = value;
									}

									if(name == 'category') {
										category_value = value;
									}

									var room_qvar = '';
									var location_qvar = '';
									var category_qvar = '';

									if(room_value != null || room_value != undefined || room_value != '') {
										var room_qvar = 'room='+room_value;
									}

									if(location_qvar != null || location_qvar != undefined || location_qvar != '') {
										var location_qvar = 'location='+location_value;
									}

									if(category_qvar != null || category_qvar != undefined || category_qvar != '') {
										var category_qvar = 'category='+category_value;
									}

									window.location.href = '<?php echo $this_page;?>?'+room_qvar+'&'+location_qvar+'&'+category_qvar;
								});
							});
						</script>

						<?php

						if(!empty($_GET)) {
							echo '<div class="s12">';
								echo '<a class="button" href="'.$this_page.'">Reset</a>';
							echo '</div>';
						}

						echo '<div class="schedule s12">';
							foreach($output as $title => $times) {


								usort($times, function($a, $b) {
									return (strtotime($a['time']) > strtotime($b['time']));
								});

								if(!empty($times)) {
									echo '<div class="day s12">';
										echo '<h4>'.ucfirst($title).'</h4>';

										foreach($times as $time) {
											echo '<div class="time row">';
												echo '<div class="s6 m3">';
													echo '<p>'.$time['time'].'</p>';
												echo '</div>';

												echo '<div class="s6 m9">';
													echo '<p class="sched-title" data-content="'.$time['content'].'" data-teacher="'.$time['teacher'].'">'.$time['title'].'</p>';
												echo '</div>';
											echo '</div>';
										}
									echo '</div>';
								}
							}

						echo '</div>';	
					echo '</div>';

				} else {
					echo '<p>Nothing here yet. Check back soon</p>';
				}

				wp_reset_query();
			?>
		</div>

		<?php get_sidebar(); ?>

	</div>

	<?php echo apply_filters('the_content', $content_blocks); ?>
<?php get_footer(); ?>