<?php
/**
 * Template Name: Contact Page Template
 * Description: Custom page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	<div class="main row" role="main">

		<div class="m8 columns">
			<h2 class="page-title"><?php the_title();?></h2>
			<?php the_post_thumbnail() ?>

			<?php echo do_shortcode('[contact_info show_all=true]'); ?>
			<hr>
			<?php if (have_posts()) : while (have_posts()) : the_post();
				the_content();
			endwhile; endif; ?>
		</div>

		<?php get_sidebar(); ?>
	</div>

<?php get_footer(); ?>