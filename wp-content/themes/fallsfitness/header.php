<?php
/**
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
?><!DOCTYPE html>

<!--[if lt IE 7]> <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html style="margin-top: 0!important;" class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html style="margin-top: 0!important;" class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html style="margin-top: 0!important;" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head >
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">

	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php
		$favicon = cw_options_get_option( '_cwo_favicon' );
		if(!empty($favicon)) { echo '<link rel="shortcut icon" type="image/png" href="'.$favicon.'"/>'; }
	?>

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="author" href="<?php echo get_template_directory_uri(); ?>/humans.txt">
	<link rel="dns-prefetch" href="//ajax.googleapis.com">
	<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700,900" rel="stylesheet">
	
	<!-- WP_HEAD() -->
	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie.css">
	<![endif]-->

	<?php
		$tracking = cw_options_get_option( '_cwo_tracking_code' );
		if(!empty($tracking)) {
			echo $tracking;
		}
	?>
</head>

<body <?php body_class(); ?>>
	<div class="off-canvas-wrap">
		<div class="off-canvas-wrap-inner">
		<?php get_template_part('content', 'browse-happy'); ?>

		<?php
			$dev_note = get_post_meta($post->ID, '_cwmb_dev_note', true);
			$allow = cw_options_get_option('_cwo_allow_dev_notes');

			if($allow == 'on' && !empty($dev_note)) {
				echo '<div class="dev-note">';
					echo '<span class="cwdn-open-close" title="Dev Note Available for this Page"><i class="fa fa-times fa-3x" aria-hidden="true"></i>DEV<br>NOTE</span>';
					echo '<div class="the-note">';
						echo nl2br($dev_note);
						echo '<br><img style="float: right; width: 85px; margin-top: 10px;" src="'.get_template_directory_uri().'/img/cw-dark.png" alt="Site Powered by Crane | West" />';
					echo '</div>';
				echo '</div>';
			}
		?>

		<header role="banner">
			<div class="cw-nav-cont row">
				<?php
					$logo_url = cw_options_get_option( '_cwo_logo' );
					$logo_svg = cw_options_get_option( '_cwo_logo_svg' );
				?>
				<h1 class="logo s10 m4">
					<a href="/" title="<?php bloginfo( 'name' ); ?>">
						<?php
							if(!empty($logo_svg)) {
								echo $logo_svg;
							} elseif(!empty($logo_url)) {
								echo '<img src="'.$logo_url.'" alt="'.bloginfo( 'name' ).'" />';
								echo '<span class="visually-hidden">'.get_bloginfo('name').'</span>';
							} else {
								bloginfo( 'name' );
							}
						?>
					</a>
				</h1>
				<nav class="cw-nav s2 m8" role="navigation">						
					<span class="menu-toggle" data-menu="cw-nav-ul"><i class="fa fa-bars"></i></span>
					<span class="menu-close" data-menu="cw-nav-ul"><i class="fa fa-times"></i></span>
					
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'container' => '',
								'menu_class' => 'menu cf cw-nav-ul',
								'depth' => 2,
								'fallback_cb' => 'wp_page_menu',
								// 'walker' => new Foundation_Walker_Nav_Menu() // not required, use for custom nav stuff
							)
						);
					?>
				</nav>
			</div>
		</header>