<?php
/**
 * Template Name: Home Page Template
 * Description: Custom home page template.
 * @package WordPress
 * @subpackage CW
 * @since CW 1.0
 */
get_header(); ?>
	
	<?php get_template_part('content', 'slides'); ?>

	<div role="main">
		<?php
			echo '<h2 class="page-title visually-hidden">'.get_the_title().'</h2>';
			// echo '<div class="content-container">';
			// 	the_content();
			// echo '</div>';
		?>
		<div class="row home-promos">
			<div class="m6">
				<?php cw_get_promo('home-left'); ?>
			</div>

			<div class="m6">
				<?php cw_get_promo('home-right'); ?>
			</div>
		</div>

		<div class="qlinks-facebook row">
			<div class="m7 qlinks">
				<?php get_template_part('content', 'quicklinks'); ?>
			</div>

			<div class="m5">
				<?php echo do_shortcode('[custom-facebook-feed]'); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>